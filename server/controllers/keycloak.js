"use strict";
const getLoginStatusAndProfile = require("../utils/get-login-status-and-profile");
const getProfile = require("../utils/get-profile");
const { fetchAccessToken } = require("../utils/tokens/fetch-tokens");
const { refresh } = require("../utils/tokens/refresh");

/**
 * A set of functions called "actions" for `keycloak`
 */

module.exports = {
  index: async (ctx) => {
    ctx.body = "The Keycloak plugin is running.";
  },
  login: async (ctx) => {
    ctx.set("Cache-Control", "no-cache, no-store, must-revalidate");

    const {
      debug,
      clientId,
      redirectUri,
      authEndpoint,
      additionalScopeEntries,
    } = strapi.config.keycloak;

    let scope = "openid profile offline_access";

    if (additionalScopeEntries?.length > 0) {
      scope += " " + additionalScopeEntries.join(" ");
    }

    debug &&
      strapi.log.info(`[KEYCLOAK] Login initiated. Started new session.`);

    debug &&
      strapi.log.info(
        `[KEYCLOAK] Redirect URL after login is set to ${ctx.query.redirectTo}.`
      );

    const forwardUrl = `${authEndpoint}?client_id=${clientId}&redirect_uri=${redirectUri}&scope=${scope}&response_type=code&state=${encodeURIComponent(
      ctx.query.redirectTo
    )}`;

    debug &&
      strapi.log.info(
        `[KEYCLOAK] Now forwarding user to Keycloak: ${forwardUrl}.`
      );

    ctx.response.redirect(forwardUrl);
  },
  callback: async (ctx) => {
    ctx.set("Cache-Control", "no-cache, no-store, must-revalidate");
    const {
      debug,
      redirectToUrlAfterLogin,
      permittedOverwriteRedirectUrls = [],
      onLoginSuccessful,
      onLoginFailed,
      canLogin,
    } = strapi.config.keycloak;

    debug && strapi.log.info(`[KEYCLOAK] Callback received.`);

    // Strapi sometimes does not include the host name and protocol, making it an invalid URL.
    // With this code, we clean that up.
    const cleanUrl = ctx.req.url.startsWith("http")
      ? ctx.req.url
      : `http://example.com${ctx.req.url}`;

    const requestUrl = new URL(cleanUrl);
    const code = requestUrl.searchParams.get("code");
    const state = requestUrl.searchParams.get("state");

    const { accessToken, refreshToken, idToken, error } =
      await fetchAccessToken({ code });

    debug &&
      strapi.log.info(`[KEYCLOAK] Code handed over to callback: ${code}.`);

    if (accessToken && idToken && !error) {
      const overwriteRedirectUrl = state;

      debug &&
        strapi.log.info(
          `[KEYCLOAK] Overwrite redirect URL is ${overwriteRedirectUrl}.`
        );

      ctx.session.keycloak = {
        accessToken,
        idToken,
        refreshToken,
      };

      // call once to make sure the profile is stored in the session
      await getProfile(ctx);

      let redirectUrl = redirectToUrlAfterLogin;

      // allow URL override only for permitted URLs
      if (
        overwriteRedirectUrl &&
        permittedOverwriteRedirectUrls.find((permittedUrl) =>
          overwriteRedirectUrl.startsWith(permittedUrl)
        )
      ) {
        redirectUrl = overwriteRedirectUrl;
      }

      if (canLogin) {
        const userProfile = await getProfile(ctx);
        if (!(await canLogin(userProfile))) {
          delete ctx.session.keycloak;

          ctx.status = 403;

          return;
        }
      }

      debug &&
        strapi.log.info(`[KEYCLOAK] Final redirect URL is ${redirectUrl}.`);

      if (onLoginSuccessful) {
        await onLoginSuccessful?.(ctx);
      }

      if (redirectUrl != null) {
        ctx.redirect(redirectUrl);
        return;
      }

      ctx.body = "Welcome!";
    } else {
      strapi.log.warn(
        "[KEYCLOAK] Error retrieving token from Keycloak: " +
          JSON.stringify(error)
      );
      if (onLoginFailed) {
        await onLoginFailed(ctx);
      }
      ctx.body = "Error logging in: ";
    }
  },
  refresh: async (ctx) => {
    ctx.set("Cache-Control", "no-cache, no-store, must-revalidate");
    await refresh(ctx);
    return "";
  },
  logout: (ctx) => {
    ctx.set("Cache-Control", "no-cache, no-store, must-revalidate");

    const { logoutEndpoint, redirectToUrlAfterLogout } = strapi.config.keycloak;

    const idToken = ctx.session?.keycloak?.idToken;

    if (ctx.session?.keycloak) {
      delete ctx.session.keycloak;
    }

    const redirectUrl = ctx.query.redirectTo || redirectToUrlAfterLogout || "";

    if (logoutEndpoint) {
      ctx.redirect(
        `${logoutEndpoint}?post_logout_redirect_uri=${redirectUrl}&id_token_hint=${idToken}`
      );
    } else {
      ctx.redirect(redirectUrl);
    }
  },
  isLoggedIn: async (ctx) => {
    ctx.set("Cache-Control", "no-cache, no-store, must-revalidate");

    const { isLoggedIn } = await getLoginStatusAndProfile(ctx);
    ctx.body = isLoggedIn;
  },
  profile: async (ctx) => {
    ctx.set("Cache-Control", "no-cache, no-store, must-revalidate");

    let { isLoggedIn, profile } = await getLoginStatusAndProfile(ctx);

    if (!isLoggedIn) {
      ctx.status = 403;
      return;
    }

    ctx.body = profile;
  },
};
