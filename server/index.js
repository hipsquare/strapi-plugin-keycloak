"use strict";

const controllers = require("./controllers");
const routes = require("./routes");
const middlewares = require("./middlewares");
const { loadWellknownConfig } = require("./utils/load-wellknown-config");

module.exports = {
  controllers,
  routes,
  middlewares,
};

void loadWellknownConfig();
