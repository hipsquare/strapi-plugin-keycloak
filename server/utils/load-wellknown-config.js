const fetch = require("node-fetch");

async function loadWellknownConfig() {
  const { wellknownUrl, debug } = strapi.config.keycloak ?? {};

  if (!wellknownUrl) {
    return;
  }

  debug &&
    strapi.log.info(
      `[KEYCLOAK] Retrieving config from wellknown URL ${wellknownUrl}`
    );

  const result = await fetch(wellknownUrl).then((res) => res.json());

  debug &&
    strapi.log.info(
      `[KEYCLOAK] Retrieved config from wellknown URL, setting it now`
    );

  strapi.config.keycloak = {
    ...strapi.config.keycloak,
    tokenEndpoint: result.token_endpoint,
    authEndpoint: result.authorization_endpoint,
    userinfoEndpoint: result.userinfo_endpoint,
    logoutEndpoint: result.end_session_endpoint,
    jwksUri: result.jwks_uri,
  };
}

module.exports = { loadWellknownConfig };
