const { refreshAccessToken } = require("./fetch-tokens");
const { getTokenFromContext } = require("./get-token-from-context");

async function refresh(ctx) {
  const { debug } = strapi.config.keycloak;

  const refreshToken = getTokenFromContext(ctx, "refreshToken");

  const {
    accessToken,
    refreshToken: newRefreshToken,
    idToken,
    error,
  } = await refreshAccessToken({ refreshToken });

  if (error) {
    ctx.status = 400;
    ctx.body = "Error refreshing token.";
    debug && strapi.log.info(`[KEYCLOAK] Error refreshing token: ${error}.`);
    return;
  }

  ctx.session.keycloak = {
    accessToken,
    idToken,
    refreshToken: newRefreshToken,
  };
}

module.exports = { refresh };
