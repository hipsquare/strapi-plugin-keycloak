const fetch = require("node-fetch");
const { getJwtValidityAndPayload } = require("./jwt");
const { getTokenFromContext } = require("./tokens/get-token-from-context");

module.exports = async (ctx) => {
  if (ctx.session?.keycloak?.profile) {
    return ctx.session.keycloak.profile;
  }

  const { afterRetrieveProfile, onRetrieveProfile } = strapi.config.keycloak;

  const {
    userinfoEndpoint,
    alwaysFetchProfileFromKeycloak,
    jwtPublicKey,
    jwtAlgorithm = "RS256",
    jwksUri,
  } = strapi.config.keycloak;

  const accessToken = getTokenFromContext(ctx, "accessToken");
  const idToken = getTokenFromContext(ctx, "idToken");

  if (!accessToken) {
    return null;
  }

  let accessTokenPayload = {},
    idTokenPayload = {};

  if (jwtPublicKey || jwksUri) {
    const accessTokenCheckResult = await getJwtValidityAndPayload(accessToken, {
      algorithm: jwtAlgorithm,
      publicKey: jwtPublicKey,
      jwksUri,
    });

    accessTokenPayload = accessTokenCheckResult.payload;

    const idTokenCheckResult = await getJwtValidityAndPayload(idToken, {
      algorithm: jwtAlgorithm,
      publicKey: jwtPublicKey,
      jwksUri,
    });

    if (!idTokenCheckResult.valid) {
      // if none of the tokens is valid, we don't have a successful login
      return;
    }

    idTokenPayload = idTokenCheckResult.payload;

    if (!alwaysFetchProfileFromKeycloak) {
      return {
        ...accessTokenPayload,
        ...idTokenPayload,
      };
    }
  }

  // if no ways to verify the tokens are set, fall back to contacting Keycloak for details

  // verify that user is valid by getting user info
  const userInfoResponse = await fetch(userinfoEndpoint, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });

  const userInfoResponseBody = await userInfoResponse.json();

  if (!userInfoResponseBody) {
    return;
  }

  let userInfo = {
    ...userInfoResponseBody,
    ...accessTokenPayload,
    ...idTokenPayload,
  };

  if (onRetrieveProfile) {
    const additionalProfileInfos = await onRetrieveProfile(ctx);
    userInfo = {
      ...userInfo,
      ...additionalProfileInfos,
    };
  }

  if (afterRetrieveProfile) {
    await afterRetrieveProfile(ctx, userInfo);
  }

  ctx.session.keycloak.profile = userInfo;

  return userInfo;
};
