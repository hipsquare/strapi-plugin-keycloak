# Changelog

## 1.3.0

If no `logoutEndpoint` is defined, the plugin will now just end the Strapi session, but not redirect the user to the IdP's `end_session` endpoint.

## 1.2.0

We added support for `.well-known` URLs to retrieve config information from.

## 1.1.0

You can now define additional scope entries for token retrieval using the `additionalScopeEntries` configuration property.

## 1.0.0

With this release comes a **breaking change**: We have **removed the support for passing tokens to the frontend**. With the advent of the [Capacitor Cookies plugin](https://capacitorjs.com/docs/apis/cookies), this previously required workaround is not required anymore. We now fully rely on the much safer Koa sessions, which use HTTP only cookies rather than exposing tokens to the frontend.

## 0.1.0

This release contains a **breaking change**:

1. Instead of directly handing over the access and refresh tokens issued by the IdP to SPA frontend applications, the plugin now generates a new JSON Web Token which contains the three IdP tokens in its payload. This simplifies a couple of processes and allows the `/profile` endpoint to contain information contained in the ID token's payload.<br/><br/>
   Please supply a `pluginJwtSecret` configuration entry that is used as the secret key to sign the plugin JWT. If none is provided, we will use a random string that is generated on startup. While this will work as long as the instance is running, it also means that JWTs will be invalid after a Strapi restart. So please make sure to supply the value.
2. As we introduced the strapi oidc token, we have renamed some the `appendAccessTokenToRedirectUrlAfterLogin` to `appendStrapiOidcTokenToRedirectUrlAfterLogin`, as well as the `accessToken` parameter that is forwarded to the frontend to `strapiOidcToken`.

## 0.0.25

Use `post_logout_redirect_uri` instead of `redirect_uri` for `logout` redirects according to the [OpenID specification](https://openid.net/specs/openid-connect-rpinitiated-1_0.html#RPLogout). This is required especially after the upgrade to Keycloak 18.

## 0.0.24

Fixed a bug where the logout would not actually log the user out.

## 0.0.23

Add the `jwtPublicKey` and `jwtAlgorithm` configuration properties to get the login status and the user profile from the access token instead of requiring a fetch from the Keycloak `userinfo` endpoint each time.

## 0.0.22

When processing Strapi API tokens, treat the "Bearer" portion of the code without case sensitivity (e.g. `bearer` and `Bearer` are both accepted).

## 0.0.21

The middleware now checks for Strapi API tokens and, if one is set, doesn't require a Keycloak login. This is in line with the way that Strapi API tokens are designed: To bypass other authorization strategies.

## 0.0.20

Populate `ctx.state.keycloak.profile` with the current user's Keycloak profile. Using that, you can access user information in Strapi code.

## 0.0.19

Introduced `redirectTo` query parameter to the `logout` endpoint.
